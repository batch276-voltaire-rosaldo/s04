# Activity #4

class Animal():
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def eat(self):
        pass

    def make_sound(self):
        pass

class Dog(Animal):
    def __init__(self, name, breed, age, food):
        super().__init__(name, breed, age)

        self._food = food

    def eat(self, food):
        print(f'Eaten {self._food}')
    def make_sound(self):
        print('Bark! Woof! Arf!')
    def call(self):
        print(f'Here {self._name}!')

class Cat(Animal):
    def __init__(self, name, breed, age, food):
        super().__init__(name, breed, age)

        self._food = food

    def eat(self, food):
        print(f'Serve me {self._food}')
    def make_sound(self):
        print('Miaow! Nyaw! Nyaaaaa!')
    def call(self):
        print(f'{self._name}, come on!')

dog1 = Dog('Isis', 'Dalmatian', 15, 'Steak')
dog1.eat('Steak')
dog1.make_sound()
dog1.call()

cat1 = Cat('Puss', 'Persian', 4, 'Tuna')
cat1.eat('Tuna')
cat1.make_sound()
cat1.call()